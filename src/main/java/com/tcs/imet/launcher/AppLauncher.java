package com.tcs.imet.launcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.tcs.imet")
public class AppLauncher {
	public static void main(String[] args) {
		SpringApplication.run(AppLauncher.class, args);
	}
}