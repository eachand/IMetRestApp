package com.tcs.imet.pojo;

public class ServiceNowRequest {

	java.lang.Boolean active;

	java.lang.String activityDue;

	java.lang.String additionalAssigneeList;

	java.lang.String approval;

	java.lang.String approvalHistory;

	java.lang.String approvalSet;

	java.lang.String assignedTo;

	java.lang.String assignmentGroup;

	java.lang.String businessDuration;

	java.lang.String businessService;

	java.math.BigInteger businessStc;

	java.lang.String calendarDuration;

	java.math.BigInteger calendarStc;

	java.lang.String callerId;

	java.lang.String category;

	java.lang.String causedBy;

	java.math.BigInteger childIncidents;

	java.lang.String closeCode;

	java.lang.String closeNotes;

	java.lang.String closedAt;

	java.lang.String closedBy;

	java.lang.String cmdbCi;

	java.lang.String comments;

	java.lang.String commentsAndWorkNotes;

	java.lang.String company;

	java.lang.String contactType;

	java.lang.String correlationDisplay;

	java.lang.String correlationId;

	java.lang.String deliveryPlan;

	java.lang.String deliveryTask;

	java.lang.String description;

	java.lang.String dueDate;

	java.math.BigInteger escalation;

	java.lang.String expectedStart;

	java.lang.String followUp;

	java.lang.String groupList;

	java.math.BigInteger holdReason;

	java.math.BigInteger impact;

	java.math.BigInteger incidentState;

	java.lang.Boolean knowledge;

	java.lang.String location;

	java.lang.Boolean madeSla;

	java.math.BigInteger notify;

	java.lang.String number;

	java.lang.String openedAt;

	java.lang.String openedBy;

	java.math.BigInteger order;

	java.lang.String parent;

	java.lang.String parentIncident;

	java.math.BigInteger priority;

	java.lang.String problemId;

	java.math.BigInteger reassignmentCount;

	java.math.BigInteger reopenCount;

	java.lang.String resolvedAt;

	java.lang.String resolvedBy;

	java.lang.String rfc;

	java.math.BigInteger severity;

	java.lang.String shortDescription;

	java.lang.String slaDue;

	java.math.BigInteger state;

	java.lang.String subcategory;

	java.lang.String sysClassName;

	java.lang.String sysCreatedBy;

	java.lang.String sysCreatedOn;

	java.lang.String sysDomain;

	java.lang.String sysDomainPath;

	java.lang.String sysId;

	java.math.BigInteger sysModCount;

	java.lang.String sysUpdatedBy;

	java.lang.String sysUpdatedOn;

	java.lang.String timeWorked;

	java.lang.String uponApproval;

	java.lang.String uponReject;

	java.math.BigInteger urgency;

	java.lang.String userInput;

	java.lang.String watchList;

	java.lang.String workEnd;

	java.lang.String workNotes;

	java.lang.String workNotesList;

	java.lang.String workStart;

	java.lang.String useView;

	java.lang.String encodedQuery;

	java.lang.String limit;

	java.lang.String firstRow;

	java.lang.String lastRow;

	java.lang.String orderBy;

	java.lang.String orderByDesc;

	java.lang.String excludeColumns;

	public ServiceNowRequest(String openedAt, String closedAt) {
		this.openedAt = openedAt;
		this.closedAt = closedAt;
	}

	public java.lang.Boolean getActive() {
		return active;
	}

	public void setActive(java.lang.Boolean active) {
		this.active = active;
	}

	public java.lang.String getActivityDue() {
		return activityDue;
	}

	public void setActivityDue(java.lang.String activityDue) {
		this.activityDue = activityDue;
	}

	public java.lang.String getAdditionalAssigneeList() {
		return additionalAssigneeList;
	}

	public void setAdditionalAssigneeList(java.lang.String additionalAssigneeList) {
		this.additionalAssigneeList = additionalAssigneeList;
	}

	public java.lang.String getApproval() {
		return approval;
	}

	public void setApproval(java.lang.String approval) {
		this.approval = approval;
	}

	public java.lang.String getApprovalHistory() {
		return approvalHistory;
	}

	public void setApprovalHistory(java.lang.String approvalHistory) {
		this.approvalHistory = approvalHistory;
	}

	public java.lang.String getApprovalSet() {
		return approvalSet;
	}

	public void setApprovalSet(java.lang.String approvalSet) {
		this.approvalSet = approvalSet;
	}

	public java.lang.String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(java.lang.String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public java.lang.String getAssignmentGroup() {
		return assignmentGroup;
	}

	public void setAssignmentGroup(java.lang.String assignmentGroup) {
		this.assignmentGroup = assignmentGroup;
	}

	public java.lang.String getBusinessDuration() {
		return businessDuration;
	}

	public void setBusinessDuration(java.lang.String businessDuration) {
		this.businessDuration = businessDuration;
	}

	public java.lang.String getBusinessService() {
		return businessService;
	}

	public void setBusinessService(java.lang.String businessService) {
		this.businessService = businessService;
	}

	public java.math.BigInteger getBusinessStc() {
		return businessStc;
	}

	public void setBusinessStc(java.math.BigInteger businessStc) {
		this.businessStc = businessStc;
	}

	public java.lang.String getCalendarDuration() {
		return calendarDuration;
	}

	public void setCalendarDuration(java.lang.String calendarDuration) {
		this.calendarDuration = calendarDuration;
	}

	public java.math.BigInteger getCalendarStc() {
		return calendarStc;
	}

	public void setCalendarStc(java.math.BigInteger calendarStc) {
		this.calendarStc = calendarStc;
	}

	public java.lang.String getCallerId() {
		return callerId;
	}

	public void setCallerId(java.lang.String callerId) {
		this.callerId = callerId;
	}

	public java.lang.String getCategory() {
		return category;
	}

	public void setCategory(java.lang.String category) {
		this.category = category;
	}

	public java.lang.String getCausedBy() {
		return causedBy;
	}

	public void setCausedBy(java.lang.String causedBy) {
		this.causedBy = causedBy;
	}

	public java.math.BigInteger getChildIncidents() {
		return childIncidents;
	}

	public void setChildIncidents(java.math.BigInteger childIncidents) {
		this.childIncidents = childIncidents;
	}

	public java.lang.String getCloseCode() {
		return closeCode;
	}

	public void setCloseCode(java.lang.String closeCode) {
		this.closeCode = closeCode;
	}

	public java.lang.String getCloseNotes() {
		return closeNotes;
	}

	public void setCloseNotes(java.lang.String closeNotes) {
		this.closeNotes = closeNotes;
	}

	public java.lang.String getClosedAt() {
		return closedAt;
	}

	public void setClosedAt(java.lang.String closedAt) {
		this.closedAt = closedAt;
	}

	public java.lang.String getClosedBy() {
		return closedBy;
	}

	public void setClosedBy(java.lang.String closedBy) {
		this.closedBy = closedBy;
	}

	public java.lang.String getCmdbCi() {
		return cmdbCi;
	}

	public void setCmdbCi(java.lang.String cmdbCi) {
		this.cmdbCi = cmdbCi;
	}

	public java.lang.String getComments() {
		return comments;
	}

	public void setComments(java.lang.String comments) {
		this.comments = comments;
	}

	public java.lang.String getCommentsAndWorkNotes() {
		return commentsAndWorkNotes;
	}

	public void setCommentsAndWorkNotes(java.lang.String commentsAndWorkNotes) {
		this.commentsAndWorkNotes = commentsAndWorkNotes;
	}

	public java.lang.String getCompany() {
		return company;
	}

	public void setCompany(java.lang.String company) {
		this.company = company;
	}

	public java.lang.String getContactType() {
		return contactType;
	}

	public void setContactType(java.lang.String contactType) {
		this.contactType = contactType;
	}

	public java.lang.String getCorrelationDisplay() {
		return correlationDisplay;
	}

	public void setCorrelationDisplay(java.lang.String correlationDisplay) {
		this.correlationDisplay = correlationDisplay;
	}

	public java.lang.String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(java.lang.String correlationId) {
		this.correlationId = correlationId;
	}

	public java.lang.String getDeliveryPlan() {
		return deliveryPlan;
	}

	public void setDeliveryPlan(java.lang.String deliveryPlan) {
		this.deliveryPlan = deliveryPlan;
	}

	public java.lang.String getDeliveryTask() {
		return deliveryTask;
	}

	public void setDeliveryTask(java.lang.String deliveryTask) {
		this.deliveryTask = deliveryTask;
	}

	public java.lang.String getDescription() {
		return description;
	}

	public void setDescription(java.lang.String description) {
		this.description = description;
	}

	public java.lang.String getDueDate() {
		return dueDate;
	}

	public void setDueDate(java.lang.String dueDate) {
		this.dueDate = dueDate;
	}

	public java.math.BigInteger getEscalation() {
		return escalation;
	}

	public void setEscalation(java.math.BigInteger escalation) {
		this.escalation = escalation;
	}

	public java.lang.String getExpectedStart() {
		return expectedStart;
	}

	public void setExpectedStart(java.lang.String expectedStart) {
		this.expectedStart = expectedStart;
	}

	public java.lang.String getFollowUp() {
		return followUp;
	}

	public void setFollowUp(java.lang.String followUp) {
		this.followUp = followUp;
	}

	public java.lang.String getGroupList() {
		return groupList;
	}

	public void setGroupList(java.lang.String groupList) {
		this.groupList = groupList;
	}

	public java.math.BigInteger getHoldReason() {
		return holdReason;
	}

	public void setHoldReason(java.math.BigInteger holdReason) {
		this.holdReason = holdReason;
	}

	public java.math.BigInteger getImpact() {
		return impact;
	}

	public void setImpact(java.math.BigInteger impact) {
		this.impact = impact;
	}

	public java.math.BigInteger getIncidentState() {
		return incidentState;
	}

	public void setIncidentState(java.math.BigInteger incidentState) {
		this.incidentState = incidentState;
	}

	public java.lang.Boolean getKnowledge() {
		return knowledge;
	}

	public void setKnowledge(java.lang.Boolean knowledge) {
		this.knowledge = knowledge;
	}

	public java.lang.String getLocation() {
		return location;
	}

	public void setLocation(java.lang.String location) {
		this.location = location;
	}

	public java.lang.Boolean getMadeSla() {
		return madeSla;
	}

	public void setMadeSla(java.lang.Boolean madeSla) {
		this.madeSla = madeSla;
	}

	public java.math.BigInteger getNotify() {
		return notify;
	}

	public void setNotify(java.math.BigInteger notify) {
		this.notify = notify;
	}

	public java.lang.String getNumber() {
		return number;
	}

	public void setNumber(java.lang.String number) {
		this.number = number;
	}

	public java.lang.String getOpenedAt() {
		return openedAt;
	}

	public void setOpenedAt(java.lang.String openedAt) {
		this.openedAt = openedAt;
	}

	public java.lang.String getOpenedBy() {
		return openedBy;
	}

	public void setOpenedBy(java.lang.String openedBy) {
		this.openedBy = openedBy;
	}

	public java.math.BigInteger getOrder() {
		return order;
	}

	public void setOrder(java.math.BigInteger order) {
		this.order = order;
	}

	public java.lang.String getParent() {
		return parent;
	}

	public void setParent(java.lang.String parent) {
		this.parent = parent;
	}

	public java.lang.String getParentIncident() {
		return parentIncident;
	}

	public void setParentIncident(java.lang.String parentIncident) {
		this.parentIncident = parentIncident;
	}

	public java.math.BigInteger getPriority() {
		return priority;
	}

	public void setPriority(java.math.BigInteger priority) {
		this.priority = priority;
	}

	public java.lang.String getProblemId() {
		return problemId;
	}

	public void setProblemId(java.lang.String problemId) {
		this.problemId = problemId;
	}

	public java.math.BigInteger getReassignmentCount() {
		return reassignmentCount;
	}

	public void setReassignmentCount(java.math.BigInteger reassignmentCount) {
		this.reassignmentCount = reassignmentCount;
	}

	public java.math.BigInteger getReopenCount() {
		return reopenCount;
	}

	public void setReopenCount(java.math.BigInteger reopenCount) {
		this.reopenCount = reopenCount;
	}

	public java.lang.String getResolvedAt() {
		return resolvedAt;
	}

	public void setResolvedAt(java.lang.String resolvedAt) {
		this.resolvedAt = resolvedAt;
	}

	public java.lang.String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(java.lang.String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public java.lang.String getRfc() {
		return rfc;
	}

	public void setRfc(java.lang.String rfc) {
		this.rfc = rfc;
	}

	public java.math.BigInteger getSeverity() {
		return severity;
	}

	public void setSeverity(java.math.BigInteger severity) {
		this.severity = severity;
	}

	public java.lang.String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(java.lang.String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public java.lang.String getSlaDue() {
		return slaDue;
	}

	public void setSlaDue(java.lang.String slaDue) {
		this.slaDue = slaDue;
	}

	public java.math.BigInteger getState() {
		return state;
	}

	public void setState(java.math.BigInteger state) {
		this.state = state;
	}

	public java.lang.String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(java.lang.String subcategory) {
		this.subcategory = subcategory;
	}

	public java.lang.String getSysClassName() {
		return sysClassName;
	}

	public void setSysClassName(java.lang.String sysClassName) {
		this.sysClassName = sysClassName;
	}

	public java.lang.String getSysCreatedBy() {
		return sysCreatedBy;
	}

	public void setSysCreatedBy(java.lang.String sysCreatedBy) {
		this.sysCreatedBy = sysCreatedBy;
	}

	public java.lang.String getSysCreatedOn() {
		return sysCreatedOn;
	}

	public void setSysCreatedOn(java.lang.String sysCreatedOn) {
		this.sysCreatedOn = sysCreatedOn;
	}

	public java.lang.String getSysDomain() {
		return sysDomain;
	}

	public void setSysDomain(java.lang.String sysDomain) {
		this.sysDomain = sysDomain;
	}

	public java.lang.String getSysDomainPath() {
		return sysDomainPath;
	}

	public void setSysDomainPath(java.lang.String sysDomainPath) {
		this.sysDomainPath = sysDomainPath;
	}

	public java.lang.String getSysId() {
		return sysId;
	}

	public void setSysId(java.lang.String sysId) {
		this.sysId = sysId;
	}

	public java.math.BigInteger getSysModCount() {
		return sysModCount;
	}

	public void setSysModCount(java.math.BigInteger sysModCount) {
		this.sysModCount = sysModCount;
	}

	public java.lang.String getSysUpdatedBy() {
		return sysUpdatedBy;
	}

	public void setSysUpdatedBy(java.lang.String sysUpdatedBy) {
		this.sysUpdatedBy = sysUpdatedBy;
	}

	public java.lang.String getSysUpdatedOn() {
		return sysUpdatedOn;
	}

	public void setSysUpdatedOn(java.lang.String sysUpdatedOn) {
		this.sysUpdatedOn = sysUpdatedOn;
	}

	public java.lang.String getTimeWorked() {
		return timeWorked;
	}

	public void setTimeWorked(java.lang.String timeWorked) {
		this.timeWorked = timeWorked;
	}

	public java.lang.String getUponApproval() {
		return uponApproval;
	}

	public void setUponApproval(java.lang.String uponApproval) {
		this.uponApproval = uponApproval;
	}

	public java.lang.String getUponReject() {
		return uponReject;
	}

	public void setUponReject(java.lang.String uponReject) {
		this.uponReject = uponReject;
	}

	public java.math.BigInteger getUrgency() {
		return urgency;
	}

	public void setUrgency(java.math.BigInteger urgency) {
		this.urgency = urgency;
	}

	public java.lang.String getUserInput() {
		return userInput;
	}

	public void setUserInput(java.lang.String userInput) {
		this.userInput = userInput;
	}

	public java.lang.String getWatchList() {
		return watchList;
	}

	public void setWatchList(java.lang.String watchList) {
		this.watchList = watchList;
	}

	public java.lang.String getWorkEnd() {
		return workEnd;
	}

	public void setWorkEnd(java.lang.String workEnd) {
		this.workEnd = workEnd;
	}

	public java.lang.String getWorkNotes() {
		return workNotes;
	}

	public void setWorkNotes(java.lang.String workNotes) {
		this.workNotes = workNotes;
	}

	public java.lang.String getWorkNotesList() {
		return workNotesList;
	}

	public void setWorkNotesList(java.lang.String workNotesList) {
		this.workNotesList = workNotesList;
	}

	public java.lang.String getWorkStart() {
		return workStart;
	}

	public void setWorkStart(java.lang.String workStart) {
		this.workStart = workStart;
	}

	public java.lang.String getUseView() {
		return useView;
	}

	public void setUseView(java.lang.String useView) {
		this.useView = useView;
	}

	public java.lang.String getEncodedQuery() {
		return encodedQuery;
	}

	public void setEncodedQuery(java.lang.String encodedQuery) {
		this.encodedQuery = encodedQuery;
	}

	public java.lang.String getLimit() {
		return limit;
	}

	public void setLimit(java.lang.String limit) {
		this.limit = limit;
	}

	public java.lang.String getFirstRow() {
		return firstRow;
	}

	public void setFirstRow(java.lang.String firstRow) {
		this.firstRow = firstRow;
	}

	public java.lang.String getLastRow() {
		return lastRow;
	}

	public void setLastRow(java.lang.String lastRow) {
		this.lastRow = lastRow;
	}

	public java.lang.String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(java.lang.String orderBy) {
		this.orderBy = orderBy;
	}

	public java.lang.String getOrderByDesc() {
		return orderByDesc;
	}

	public void setOrderByDesc(java.lang.String orderByDesc) {
		this.orderByDesc = orderByDesc;
	}

	public java.lang.String getExcludeColumns() {
		return excludeColumns;
	}

	public void setExcludeColumns(java.lang.String excludeColumns) {
		this.excludeColumns = excludeColumns;
	}

}
