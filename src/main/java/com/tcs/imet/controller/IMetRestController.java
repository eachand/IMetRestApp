package com.tcs.imet.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.peregrine.servicecenter.pws.RetrieveIncidentRequest;
import com.peregrine.servicecenter.pws.RetrieveIncidentResponse;
import com.service_now.incident.GetRecordsResponse.GetRecordsResult;
import com.tcs.imet.delegator.HPSMDelegator;
import com.tcs.imet.delegator.RemedyDelegator;
import com.tcs.imet.delegator.ServiceNowDelegator;
import com.tcs.imet.pojo.RemedyRequest;
import com.tcs.imet.pojo.ServiceNowRequest;

@RestController
public class IMetRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(IMetRestController.class);

	@Autowired
	private ServiceNowDelegator serviceNowDelegator;

	@Autowired
	private HPSMDelegator hpsmDelegator;

	@Autowired
	private RemedyDelegator remedyDelegator;

	@RequestMapping(value = "/getRemedyRecords", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json")
	public List<hpd_incidentinterface_ws.GetListOutputMap.GetListValues> getRemedyRecords(String openedAt,
			String closedAt) {
		LOGGER.info("Remedy - openedAt: " + openedAt);
		LOGGER.info("Remedy - closedAt: " + closedAt);
		return remedyDelegator.helpDeskQueryListService(new RemedyRequest());
	}

	@RequestMapping(value = "/getHPSMRecords", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json")
	public RetrieveIncidentResponse getHPSMRecords(String openedAt, String closedAt) {
		LOGGER.info("HPSM - openedAt: " + openedAt);
		LOGGER.info("HPSM - closedAt: " + closedAt);
		return hpsmDelegator.retrieveIncidents(new RetrieveIncidentRequest());
	}

	@RequestMapping(value = "/getServiceNowRecords", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json")
	public List<GetRecordsResult> getServiceNowRecords(String openedAt, String closedAt) {
		LOGGER.info("ServiceNow - openedAt: " + openedAt);
		LOGGER.info("ServiceNow - closedAt: " + closedAt);
		return serviceNowDelegator.getRecords(new ServiceNowRequest(openedAt, closedAt));
	}

}