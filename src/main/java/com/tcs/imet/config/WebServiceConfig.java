package com.tcs.imet.config;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.peregrine.servicecenter.pws.IncidentManagement;
import com.service_now.incident.ServiceNowSoap;

import hpd_incidentinterface_ws.HPDIncidentInterfaceWSPortTypePortType;

@Configuration
public class WebServiceConfig {

	@Profile("mock")
	@Configuration
	public class Mock {
		@Bean(name = "serviceNowClient")
		public ServiceNowSoap serviceNowClient() {
			String address = "http://localhost:8111/ServiceNowMock";
			// https://dev37275.service-now.com/incident.do?SOAP
			JaxWsProxyFactoryBean factoryBean = new JaxWsProxyFactoryBean();
			factoryBean.setServiceClass(ServiceNowSoap.class);
			factoryBean.setAddress(address);

			return (ServiceNowSoap) factoryBean.create();
		}

		@Bean(name = "hpsmService")
		public IncidentManagement hpsmService() {
			String address = "http://localhost:8089/mockIncidentManagement";
			JaxWsProxyFactoryBean factoryBean = new JaxWsProxyFactoryBean();
			factoryBean.setServiceClass(IncidentManagement.class);
			factoryBean.setAddress(address);

			return (IncidentManagement) factoryBean.create();
		}

		@Bean(name = "remedyService")
		public HPDIncidentInterfaceWSPortTypePortType remedyService() {
			String address = "http://localhost:8112/mockRemedy";
			JaxWsProxyFactoryBean factoryBean = new JaxWsProxyFactoryBean();
			factoryBean.setServiceClass(HPDIncidentInterfaceWSPortTypePortType.class);
			factoryBean.setAddress(address);

			return (HPDIncidentInterfaceWSPortTypePortType) factoryBean.create();
		}
	}

	@Profile("actual")
	@Configuration
	public class Actual {
		@Bean(name = "serviceNowClient")
		public ServiceNowSoap serviceNowClient() {
			String address = "https://dev37275.service-now.com/incident.do?SOAP";
			JaxWsProxyFactoryBean factoryBean = new JaxWsProxyFactoryBean();
			factoryBean.setServiceClass(ServiceNowSoap.class);
			factoryBean.setAddress(address);
			factoryBean.setUsername("admin");
			factoryBean.setPassword("Password1");

			return (ServiceNowSoap) factoryBean.create();
		}

		@Bean(name = "hpsmService")
		public IncidentManagement hpsmService() {
			String address = "http://localhost:8089/mockIncidentManagement";
			JaxWsProxyFactoryBean factoryBean = new JaxWsProxyFactoryBean();
			factoryBean.setServiceClass(IncidentManagement.class);
			factoryBean.setAddress(address);

			return (IncidentManagement) factoryBean.create();
		}

		@Bean(name = "remedyService")
		public HPDIncidentInterfaceWSPortTypePortType remedyService() {
			String address = "http://localhost:8112/mockRemedy";
			JaxWsProxyFactoryBean factoryBean = new JaxWsProxyFactoryBean();
			factoryBean.setServiceClass(HPDIncidentInterfaceWSPortTypePortType.class);
			factoryBean.setAddress(address);

			return (HPDIncidentInterfaceWSPortTypePortType) factoryBean.create();
		}
	}

}