package com.tcs.imet.delegator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.service_now.incident.GetRecordsResponse.GetRecordsResult;
import com.service_now.incident.ServiceNowSoap;
import com.tcs.imet.pojo.ServiceNowRequest;

@Component
public class ServiceNowDelegator {

	@Autowired
	private ServiceNowSoap serviceNowClient;

	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceNowDelegator.class);

	public List<GetRecordsResult> getRecords(ServiceNowRequest serviceNowRequest) {
		List<GetRecordsResult> response = null;
		try {
			response = serviceNowClient.getRecords(serviceNowRequest.getActive(), serviceNowRequest.getActivityDue(),
					serviceNowRequest.getAdditionalAssigneeList(), serviceNowRequest.getApproval(),
					serviceNowRequest.getApprovalHistory(), serviceNowRequest.getApprovalSet(),
					serviceNowRequest.getAssignedTo(), serviceNowRequest.getAssignmentGroup(),
					serviceNowRequest.getBusinessDuration(), serviceNowRequest.getBusinessService(),
					serviceNowRequest.getBusinessStc(), serviceNowRequest.getCalendarDuration(),
					serviceNowRequest.getCalendarStc(), serviceNowRequest.getCallerId(),
					serviceNowRequest.getCategory(), serviceNowRequest.getCausedBy(),
					serviceNowRequest.getChildIncidents(), serviceNowRequest.getCloseCode(),
					serviceNowRequest.getCloseNotes(), serviceNowRequest.getClosedAt(), serviceNowRequest.getClosedBy(),
					serviceNowRequest.getCmdbCi(), serviceNowRequest.getComments(),
					serviceNowRequest.getCommentsAndWorkNotes(), serviceNowRequest.getCompany(),
					serviceNowRequest.getContactType(), serviceNowRequest.getCorrelationDisplay(),
					serviceNowRequest.getCorrelationId(), serviceNowRequest.getDeliveryPlan(),
					serviceNowRequest.getDeliveryTask(), serviceNowRequest.getDescription(),
					serviceNowRequest.getDueDate(), serviceNowRequest.getEscalation(),
					serviceNowRequest.getExpectedStart(), serviceNowRequest.getFollowUp(),
					serviceNowRequest.getGroupList(), serviceNowRequest.getHoldReason(), serviceNowRequest.getImpact(),
					serviceNowRequest.getIncidentState(), serviceNowRequest.getKnowledge(),
					serviceNowRequest.getLocation(), serviceNowRequest.getMadeSla(), serviceNowRequest.getNotify(),
					serviceNowRequest.getNumber(), serviceNowRequest.getOpenedAt(), serviceNowRequest.getOpenedBy(),
					serviceNowRequest.getOrder(), serviceNowRequest.getParent(), serviceNowRequest.getParentIncident(),
					serviceNowRequest.getPriority(), serviceNowRequest.getProblemId(),
					serviceNowRequest.getReassignmentCount(), serviceNowRequest.getReopenCount(),
					serviceNowRequest.getResolvedAt(), serviceNowRequest.getResolvedBy(), serviceNowRequest.getRfc(),
					serviceNowRequest.getSeverity(), serviceNowRequest.getShortDescription(),
					serviceNowRequest.getSlaDue(), serviceNowRequest.getState(), serviceNowRequest.getSubcategory(),
					serviceNowRequest.getSysClassName(), serviceNowRequest.getSysCreatedBy(),
					serviceNowRequest.getSysCreatedOn(), serviceNowRequest.getSysDomain(),
					serviceNowRequest.getSysDomainPath(), serviceNowRequest.getSysId(),
					serviceNowRequest.getSysModCount(), serviceNowRequest.getSysUpdatedBy(),
					serviceNowRequest.getSysUpdatedOn(), serviceNowRequest.getTimeWorked(),
					serviceNowRequest.getUponApproval(), serviceNowRequest.getUponReject(),
					serviceNowRequest.getUrgency(), serviceNowRequest.getUserInput(), serviceNowRequest.getWatchList(),
					serviceNowRequest.getWorkEnd(), serviceNowRequest.getWorkNotes(),
					serviceNowRequest.getWorkNotesList(), serviceNowRequest.getWorkStart(),
					serviceNowRequest.getUseView(), serviceNowRequest.getEncodedQuery(), serviceNowRequest.getLimit(),
					serviceNowRequest.getFirstRow(), serviceNowRequest.getLastRow(), serviceNowRequest.getOrderBy(),
					serviceNowRequest.getOrderByDesc(), serviceNowRequest.getExcludeColumns());
		} catch (Exception exception) {
			LOGGER.error("ServiceNow Soap call failed!", exception);
		}
		return response;
	}

}