package com.tcs.imet.delegator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tcs.imet.pojo.RemedyRequest;

import hpd_incidentinterface_ws.HPDIncidentInterfaceWSPortTypePortType;

@Component
public class RemedyDelegator {

	@Autowired
	private HPDIncidentInterfaceWSPortTypePortType helpDeskQueryClient;

	public List<hpd_incidentinterface_ws.GetListOutputMap.GetListValues> helpDeskQueryListService(
			RemedyRequest remedyRequest) {
		List<hpd_incidentinterface_ws.GetListOutputMap.GetListValues> response = null;
		try {
			response = helpDeskQueryClient
					.helpDeskQueryListService(remedyRequest.getQualification(),
							remedyRequest.getStartRecord(),
							remedyRequest.getMaxLimit());
		} catch (Exception exception) {
		}
		return response;
	}

}